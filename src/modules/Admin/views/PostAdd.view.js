import React from 'react';
import { PostForm } from '../components/PostForm/PostForm';
import { PostFormik } from '../components/PostFormik/PostFormik';
import axios from 'axios';

function PostAddView() {
    const handleSubmit = data => {
        return axios.post('http://localhost:3010/posts', data)
    }

    // return (
    //     <PostForm
    //         btnText="Dodaj nowy"
    //         successMsg="Wpis został dodany"
    //         onSubmit={handleSubmit} 
    //     />
    // )

    return (
        <PostFormik 
            btnText="Dodaj nowy"
            successMsg="Wpis został dodany"
            onSubmit={handleSubmit} 
        />
    )
}

export {
    PostAddView,
};