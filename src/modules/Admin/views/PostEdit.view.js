import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { PostForm } from '../components/PostForm/PostForm';
import { PostFormik } from '../components/PostFormik/PostFormik';
import axios from 'axios';

function PostEditView() {
    const { id } = useParams();
    const [ postData, setPostData ] = useState(null);

    useEffect(
        () => {
            axios.get(`http://localhost:3010/posts/${id}`)
                .then(resp => setPostData(resp.data))
        },
        [id]
    )

    const handleSubmit = data => {
        return axios.patch(`http://localhost:3010/posts/${id}`, data)
    };

    if (!postData) {
        return null;
    }

    // return (
    //     <PostForm 
    //         btnText="Zapisz zmiany"
    //         successMsg="Wpis został zmieniony"
    //         initialState={postData}
    //         onSubmit={handleSubmit}
    //     />
    // )

    return (
        <PostFormik
            btnText="Zapisz zmiany"
            successMsg="Wpis został zmieniony"
            initialState={postData}
            onSubmit={handleSubmit}
        />
    )
}

export {
    PostEditView,
};