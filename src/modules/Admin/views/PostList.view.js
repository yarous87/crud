import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link, useRouteMatch } from 'react-router-dom';
import { PostList } from '../components/PostList/PostList';
import { Confirm } from '../components/Confirm/Confirm';

function PostListView() {
    const { path } = useRouteMatch();
    const [ posts, setPosts ] = useState([]);
    const [ postIdToDelete, setPostIdToDelete ] = useState(null);

    useEffect(
        () => {
            axios.get('http://localhost:3010/posts')
                .then(resp => setPosts(resp.data))
        },
        []
    )

    const handleDelete = id => {
        setPostIdToDelete(null);

        axios.delete(`http://localhost:3010/posts/${id}`)
            .then(() =>axios.get('http://localhost:3010/posts'))
            .then(resp => setPosts(resp.data))
    }


    return (
        <>
            <Link to={`${path}${path.endsWith('/') ? '' : '/'}add`}>
                Dodaj nowy
            </Link>
            <PostList
                posts={posts}
                onDelete={setPostIdToDelete}
            />
            {postIdToDelete && (
                <Confirm
                    onCancel={() => setPostIdToDelete(null)}
                    onConfirm={() => handleDelete(postIdToDelete)}
                />
            )}
        </>
    )
}

export {
    PostListView,
};