import React from 'react';import {
    Switch,
    Route,
    useRouteMatch,
} from 'react-router-dom';
import { PostListView } from './views/PostList.view.js';
import { PostAddView } from './views/PostAdd.view.js';
import { PostEditView } from './views/PostEdit.view.js';

function AdminModule() {
    const { path } = useRouteMatch();

    return (
        <Switch>
            <Route 
                path={path} 
                exact 
                component={PostListView} 
            />
            <Route 
                path={`${path}${path.endsWith('/') ? '' : '/'}add`} 
                exact 
                component={PostAddView} 
            />
            <Route 
                path={`${path}${path.endsWith('/') ? '' : '/'}edit/:id`} 
                exact 
                component={PostEditView} 
            />
        </Switch>
    )
}

export {
    AdminModule,
};