import React from 'react';
import { Link, useRouteMatch } from 'react-router-dom';

function PostList({ posts, onDelete }) {
    const { path } = useRouteMatch();

    return (
        <table>
            <tbody>
                {!!posts.length && posts.map(
                    ({ id, title }) => (
                        <tr key={id}>
                            <td>{id}</td>
                            <td>
                                <Link to={`${path}${path.endsWith('/') ? '' : '/'}edit/${id}`}>
                                    {title}
                                </Link>
                            </td>
                            <td>
                                <button onClick={() => onDelete(id)}>
                                    Usuń
                                </button>
                            </td>
                        </tr>
                    )
                )}
                {!posts.length && (
                    <tr>
                        <td>Brak wpisów</td>
                    </tr>
                )}
            </tbody>
        </table>
    )
}

export {
    PostList,
};

// export default 'test';