import React, { useState } from 'react';

const wait = time => new Promise(
    (resolve, reject) => setTimeout(resolve, time)
);

const defaultInitialState = {
    title: '',
    body: ''
};

function PostForm({ 
    initialState, 
    onSubmit,
    btnText,
    successMsg,
 }) {
    const [ formState, setFormState ] = useState(initialState || defaultInitialState);
    const [ status, setStatus ] = useState(null);

    const handleChange = ({ target }) => {
        setFormState({
            ...formState,
            [target.name]: target.value,
        });
    }

    const handleSubmit = evt => {
        evt.preventDefault();

        setStatus(null);

        onSubmit(formState)
            // .then(() => wait(6000))
            .then(() => setStatus('SUCCESS'))
            .catch(() => setStatus('ERROR'));
    }

    return (
        <form onSubmit={handleSubmit}>
            {!!status && (
                <div>
                    {status === 'SUCCESS' ? successMsg : 'Wystąpił bład spróbuj ponownie'}
                </div>
            )}
            <div>
                <label>Tytuł</label>
                <div>
                    <input
                        type="text"
                        name="title"
                        value={formState.title}
                        onChange={handleChange}
                    />
                </div>
            </div>
            <div>
                <label>Treść</label>
                <div>
                    <textarea
                        name="body"
                        value={formState.body}
                        onChange={handleChange}
                    />
                </div>
            </div>
            <button type="submit">{btnText}</button>
        </form>
    )
}

export {
    PostForm,
}