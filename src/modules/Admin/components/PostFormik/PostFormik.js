import React, { useState } from 'react';
import { Formik, Field, ErrorMessage } from 'formik';

const defaultState = {
    title: '',
    tags: '',
    body: '',
}

function PostFormik({
    btnText,
    successMsg,
    initialState,
    onSubmit
 }) {
    const [ status, setStatus ] = useState(null);

    return (
        <Formik
            initialValues={{ ...defaultState, ...initialState }}
            onSubmit={values => {
                setStatus(null);

                return onSubmit(values)
                    .then(() => setStatus('SUCCESS'))
                    .catch(() => setStatus('ERROR'));
            }}
            validate={values => {
                const errors = {};

                if (!values.title) {
                    errors.title = 'Pole wymagane'
                }

                if (!values.body) {
                    errors.body = 'Pole wymagane'
                }

                return errors;
            }}
        >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
            }) => {
                const hasErrors = !!Object.keys(errors).length;
                const isTouched = !!Object.keys(touched).length;

                return (
                    <form onSubmit={handleSubmit}>
                        {!!status && (
                            <div>
                                {status === 'SUCCESS' ? successMsg : 'Wystąpił bład spróbuj ponownie'}
                            </div>
                        )}
                        <div>
                            <label htmlFor="title">Tytuł</label>
                            <div>
                                <Field
                                    id="title"
                                    type="text"
                                    name="title"
                                />
                                <ErrorMessage 
                                    name="title" 
                                    component="div" 
                                />
                            </div>
                        </div>

                        <div>
                            <label htmlFor="tags">Tagi</label>
                            <div>
                                {/*
                                    customwa integracja z Formikiem
                                    <input
                                    type="text"
                                    name="tags"
                                    value={values.tags}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                /> */}
                                <Field
                                    id="tags"
                                    type="text"
                                    name="tags"
                                />
                                <ErrorMessage 
                                    name="tags" 
                                    component="div" 
                                />
                            </div>
                        </div>

                        <div>
                            <label htmlFor="body">Treść</label>
                            <div>
                            <Field
                                id="body"
                                component="textarea"
                                name="body"
                            />
                            <ErrorMessage 
                                name="body" 
                                component="div" 
                            />
                            </div>
                        </div>
                        <button 
                            type="submit"
                            disabled={isSubmitting || hasErrors || !isTouched}
                        >
                            {btnText}
                        </button>
                    </form>
                )
            }}
        </Formik>
    );
}

PostFormik.defaultProps = {
    initialState: {}
}

export {
    PostFormik,
}