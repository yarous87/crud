import React from 'react';
import './Confirm.scss';

function Confirm({ onCancel, onConfirm }) {
    return (
        <>
            <div className="Confirm__overlay" />
            <div className="Confirm">
                <div>
                    Czy chcesz kontynuować?
                </div>
                <div>
                    <button onClick={onCancel}>Anuluj</button>
                    <button onClick={onConfirm}>Potwierdź</button>
                </div>
            </div>
        </>
    );
}

export {
    Confirm,
}