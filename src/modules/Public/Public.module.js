import React from 'react';
import {
    Switch,
    Route,
    useRouteMatch,
} from 'react-router-dom';
import { PostListView } from './views/PostList.view.js';
import { PostView } from './views/Post.view.js';

function PublicModule() {
    const { path } = useRouteMatch();

    return (
        <Switch>
            <Route 
                path={path} 
                exact 
                component={PostListView} 
            />
            <Route 
                path={`${path}${path.endsWith('/') ? '' : '/'}:id`} 
                exact
                component={PostView} 
            />
        </Switch>
    )
}

export {
    PublicModule,
};