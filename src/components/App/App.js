import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import { AdminModule } from '../../modules/Admin/Admin.module';
import { PublicModule } from '../../modules/Public/Public.module';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/admin/post" component={AdminModule} />
        <Route path="/" component={PublicModule} />
      </Switch>
    </Router>
  );
}

export { 
  App
};
